package com.kata.temperature.captor.persistence.mapper;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.persistence.entity.SensorConfigurationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SensorConfigurationMapper {

    SensorConfigurationMapper INSTANCE = Mappers.getMapper(SensorConfigurationMapper.class);

    SensorConfiguration toModel(SensorConfigurationEntity configurationEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "requestHistory", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    SensorConfigurationEntity fromModel(SensorConfiguration configurationEntity);
}
