package com.kata.temperature.captor.persistence.adapter;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.port.spi.TemperatureSensorPersistencePort;
import com.kata.temperature.captor.persistence.entity.RequestHistoryEntity;
import com.kata.temperature.captor.persistence.entity.SensorConfigurationEntity;
import com.kata.temperature.captor.persistence.mapper.RequestHistoryMapper;
import com.kata.temperature.captor.persistence.mapper.SensorConfigurationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class TemperatureSensorPersistenceImpl implements TemperatureSensorPersistencePort {

    private static final Logger logger = LoggerFactory.getLogger(TemperatureSensorPersistenceImpl.class);

    @Inject
    EntityManager entityManager;

    @Override
    @Transactional
    public List<TemperatureRequest> loadLatestTemperatureRequests(int size) {
        return entityManager
                .createQuery("SELECT rh FROM RequestHistoryEntity rh ORDER BY rh.eventTime DESC",
                        RequestHistoryEntity.class)
                .setFirstResult(0)
                .setMaxResults(size)
                .getResultStream()
                .map(RequestHistoryMapper.INSTANCE::toTemperatureRequest)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TemperatureRequest saveTemperatureRequest(TemperatureRequest temperatureRequest,
                                                     SensorConfiguration sensorConfiguration) {
        logger.debug("Save temperature request started");
        RequestHistoryEntity historyEntity = RequestHistoryMapper.INSTANCE.fromTemperatureRequest(temperatureRequest);

        //Save provided config
        logger.debug("Saving {}", sensorConfiguration);
        saveSensorConfiguration(sensorConfiguration);

        //Load saved config and link it to history entry
        logger.debug("Loading saved sensor config");
        loadSensorConfig(sensorConfiguration).ifPresent(historyEntity::setSensorConfiguration);

        logger.debug("Persisting {} with {}", temperatureRequest, sensorConfiguration);
        entityManager.persist(historyEntity);
        logger.debug("Temperature request saved with ID: {}", historyEntity.getId());
        return RequestHistoryMapper.INSTANCE.toTemperatureRequest(historyEntity);
    }

    private Optional<SensorConfigurationEntity> loadSensorConfig(SensorConfiguration sensorConfiguration) {
        logger.debug("Loading sensor config entity from model {}", sensorConfiguration);
        Optional<SensorConfigurationEntity> loadedConfig =
                entityManager.createQuery("SELECT sc FROM SensorConfigurationEntity sc " +
                                "WHERE sc.minimumTemperatureWarmStatus = :minimum " +
                                "AND sc.maximumTemperatureWarmStatus = :maximum", SensorConfigurationEntity.class)
                        .setParameter("minimum", sensorConfiguration.getMinimumTemperatureWarmStatus())
                        .setParameter("maximum", sensorConfiguration.getMaximumTemperatureWarmStatus())
                        .setMaxResults(1)
                        .getResultStream()
                        .findFirst();

        if (loadedConfig.isPresent()) {
            logger.debug("{} loaded successfully", loadedConfig.get());
        } else {
            logger.debug("No sensor configs found");
        }

        return loadedConfig;
    }

    @Override
    public Optional<SensorConfiguration> loadLatestSensorConfiguration() {
        logger.debug("Loading the latest sensor config from DB");
        Optional<SensorConfiguration> loadedConfig = entityManager.createQuery("SELECT sc FROM SensorConfigurationEntity sc " +
                        "ORDER BY sc.createDate DESC", SensorConfigurationEntity.class)
                .setMaxResults(1)
                .getResultStream()
                .findFirst()
                .map(SensorConfigurationMapper.INSTANCE::toModel);

        if (loadedConfig.isPresent()) {
            logger.debug("{} loaded successfully", loadedConfig.get());
        } else {
            logger.debug("No sensor configs found");
        }

        return loadedConfig;
    }

    @Override
    @Transactional
    public SensorConfiguration saveSensorConfiguration(SensorConfiguration sensorConfiguration) {
        //Check if it already exists
        logger.debug("Checking if {} already exists", sensorConfiguration);
        Optional<SensorConfigurationEntity> sensorConfigurationEntity = loadSensorConfig(sensorConfiguration);
        if (sensorConfigurationEntity.isEmpty()) {
            SensorConfigurationEntity entity = SensorConfigurationMapper.INSTANCE.fromModel(sensorConfiguration);
            logger.debug("It doesn't exist. Persisting to DB");
            entityManager.persist(entity);
            logger.debug("Sensor config successfully persisted. New ID: {}", entity.getId());
            return SensorConfigurationMapper.INSTANCE.toModel(entity);
        } else {
            logger.debug("Sensor config already exists");
            return SensorConfigurationMapper.INSTANCE.toModel(sensorConfigurationEntity.get());
        }
    }
}
