package com.kata.temperature.captor.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "request_history")
public class RequestHistoryEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name = "state")
    private String state;

    @Column(name = "temperature")
    private int temperature;

    @Column(name = "created_at")
    private LocalDateTime eventTime;

    @ManyToOne
    @JoinColumn(name = "sensor_configuration_id", referencedColumnName = "id")
    SensorConfigurationEntity sensorConfiguration;
}
