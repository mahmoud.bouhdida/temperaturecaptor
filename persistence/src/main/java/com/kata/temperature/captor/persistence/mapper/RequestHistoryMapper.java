package com.kata.temperature.captor.persistence.mapper;

import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.persistence.entity.RequestHistoryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RequestHistoryMapper {

    RequestHistoryMapper INSTANCE = Mappers.getMapper(RequestHistoryMapper.class);

    @Mapping(source = "state", target = "sensorState")
    TemperatureRequest toTemperatureRequest(RequestHistoryEntity requestHistoryEntity);

    @Mapping(source = "sensorState", target = "state")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sensorConfiguration", ignore = true)
    RequestHistoryEntity fromTemperatureRequest(TemperatureRequest requestHistoryEntity);
}
