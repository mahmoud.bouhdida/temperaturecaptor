package com.kata.temperature.captor.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "sensor_configuration")
public class SensorConfigurationEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "minimum_temp_warm_status")
    private int minimumTemperatureWarmStatus;

    @Column(name = "maximum_temp_warm_status")
    private int maximumTemperatureWarmStatus;

    @OneToMany(mappedBy = "sensorConfiguration")
    private Set<RequestHistoryEntity> requestHistory;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createDate;
}
