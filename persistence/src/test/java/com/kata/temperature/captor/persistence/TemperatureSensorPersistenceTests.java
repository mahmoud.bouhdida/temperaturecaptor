package com.kata.temperature.captor.persistence;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.model.enums.SensorState;
import com.kata.temperature.captor.persistence.adapter.TemperatureSensorPersistenceImpl;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
public class TemperatureSensorPersistenceTests {

    @Inject
    TemperatureSensorPersistenceImpl sensorPersistenceImpl;

    @DisplayName("When a sensor configuration is loaded for a first time, an empty result should be returned")
    @Test
    public void should_emptySensorConfigurationReturned_when_firstTimeLoaded() {
        Optional<SensorConfiguration> sensorConfiguration = sensorPersistenceImpl.loadLatestSensorConfiguration();
        assertTrue(sensorConfiguration.isEmpty(), "The latest sensor config should be empty");
    }

    @DisplayName("When sensor configurations are set, the latest one should be returned")
    @Test
    @TestTransaction
    public void should_latestSensorConfigReturned_when_sensorConfigurationsSet() {
        SensorConfiguration sensorConfiguration1 = SensorConfiguration.getDefaultConfiguration();
        sensorPersistenceImpl.saveSensorConfiguration(sensorConfiguration1);

        int customMinimum = 15;
        int customMaximum = 30;
        SensorConfiguration sensorConfiguration2 = SensorConfiguration.forWarmRange(customMinimum, customMaximum);
        sensorPersistenceImpl.saveSensorConfiguration(sensorConfiguration2);

        Optional<SensorConfiguration> latestSensorConfiguration = sensorPersistenceImpl.loadLatestSensorConfiguration();
        assertFalse(latestSensorConfiguration.isEmpty(), "The latest sensor config should be present");

        assertAll("The latest sensor config should be correct",
                () -> assertEquals(customMinimum, latestSensorConfiguration.get().getMinimumTemperatureWarmStatus(),
                        "The minimum temperature for WARM status is incorrect"),
                () -> assertEquals(customMaximum, latestSensorConfiguration.get().getMaximumTemperatureWarmStatus(),
                        "The maximum temperature for WARM status is incorrect")
        );
    }

    @ParameterizedTest(name = "When {0} temperature requests are done, " +
            "the returned history size is {1} for a maximum size of {2}")
    @MethodSource("provideSensorHistoryTestData")
    @TestTransaction
    public void should_sensorHistoryReturnedCorrectSize_when_requested(int tempRequestsNbr, int expectedHistorySize,
                                                                       int maxHistorySize) {
        Random random = new Random();
        SensorConfiguration sensorConfiguration = SensorConfiguration.getDefaultConfiguration();

        for (int i = 0; i < tempRequestsNbr; i++) {
            int randomStatus = random.nextInt(0, 3);
            int randomTemp = random.nextInt(0, 50);
            TemperatureRequest temperatureRequest
                    = TemperatureRequest.forStatus(SensorState.values()[randomStatus], randomTemp);
            sensorPersistenceImpl.saveTemperatureRequest(temperatureRequest, sensorConfiguration);
        }

        List<TemperatureRequest> history = sensorPersistenceImpl.loadLatestTemperatureRequests(maxHistorySize);
        assertEquals(expectedHistorySize, history.size(), "History size is incorrect");
    }

    /**
     * Provide test data for sensor history test
     *
     * @return Steam of test arguments
     */
    private static Stream<Arguments> provideSensorHistoryTestData() {
        int maxHistorySize = 15;
        return Stream.of(
                Arguments.of(0, 0, maxHistorySize),
                Arguments.of(13, 13, maxHistorySize),
                Arguments.of(20, maxHistorySize, maxHistorySize)
        );
    }
}
