package com.kata.temperature.captor.api.dto;

import lombok.Builder;
import lombok.Getter;

/**
 * An error response DTO according RFC7807
 */
@Getter
@Builder
public class ErrorResponseDto {

    private String type;
    private String title;
    private String status;
    private String detail;
    private String instance;

}
