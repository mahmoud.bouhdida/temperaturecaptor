package com.kata.temperature.captor.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SensorConfigurationDto {

    private int minimumWarmTemperature;

    private int maximumWarmTemperature;

    @Override
    public String toString() {
        return "SensorConfigurationDto{" +
                "minimumWarmTemperature=" + minimumWarmTemperature +
                ", maximumWarmTemperature=" + maximumWarmTemperature +
                '}';
    }
}
