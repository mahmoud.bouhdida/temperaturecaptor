package com.kata.temperature.captor.api.resource;

import com.kata.temperature.captor.api.dto.SensorConfigurationDto;
import com.kata.temperature.captor.api.dto.TemperatureStateDto;
import com.kata.temperature.captor.api.mapper.DtoMapper;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.port.api.TemperatureSensorServicePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("/sensor")
public class SensorResource {

    private static final Logger logger = LoggerFactory.getLogger(SensorResource.class);

    @Inject
    TemperatureSensorServicePort servicePort;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/state")
    public TemperatureStateDto state() {
        logger.info("Getting temperature state from service port");
        TemperatureRequest temperature = servicePort.getCurrentTemperature();
        TemperatureStateDto temperatureStateDto = DtoMapper.INSTANCE.fromTemperatureRequest(temperature);
        logger.info("Temperature state DTO is {}", temperatureStateDto);
        return temperatureStateDto;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/history")
    public List<TemperatureStateDto> history() {
        logger.info("Getting sensor history from service port");
        List<TemperatureStateDto> historyEntries = servicePort.getLatestTemperatureRequests()
                .stream()
                .map(DtoMapper.INSTANCE::fromTemperatureRequest)
                .collect(Collectors.toList());
        logger.info("Received history of length {}", historyEntries.size());
        return historyEntries;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/configure")
    public SensorConfigurationDto configure(SensorConfigurationDto configurationRequestDto) {
        logger.info("Configuring the new WARM status limits");
        servicePort.setWarmStatusLimits(configurationRequestDto.getMinimumWarmTemperature(),
                configurationRequestDto.getMaximumWarmTemperature());
        logger.info("The new WARM status limits are applied: {}", configurationRequestDto);
        return configurationRequestDto;
    }
}