package com.kata.temperature.captor.api.exception;

import com.kata.temperature.captor.api.dto.ErrorResponseDto;
import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import io.quarkus.runtime.configuration.ProfileManager;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;

public class ExceptionMappers {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionMappers.class);

    public static final String INVALID_CONFIG_ERROR_MESSAGE = "Invalid WARM status range";

    public static final String ERROR_CODE_URI = "/errors/";

    private static final String PROD_PROFILE_VALUE = "prod";

    @ServerExceptionMapper
    public RestResponse<ErrorResponseDto> handleInvalidSensorConfigurationException
            (InvalidSensorConfigurationException exception, ContainerRequestContext requestContext) {

        logger.error("Received an invalid sensor configuration {}: {}",
                exception.getSensorConfiguration(), exception.getMessage());
        ErrorResponseDto errorResponse = ErrorResponseDto.builder()
                .type(ERROR_CODE_URI + 1)
                .title(INVALID_CONFIG_ERROR_MESSAGE)
                .detail(exception.getMessage())
                .status(Response.Status.BAD_REQUEST.getReasonPhrase())
                .instance(requestContext.getUriInfo().getPath())
                .build();

        return RestResponse.status(RestResponse.Status.BAD_REQUEST, errorResponse);
    }

    @ServerExceptionMapper
    public RestResponse<ErrorResponseDto> handleException(Exception exception, ContainerRequestContext requestContext) {
        logger.error("An unexpected exception happened", exception);

        String activeProfile = ProfileManager.getActiveProfile();
        logger.info("The active profile is {}", activeProfile);

        boolean isProd = activeProfile.equals(PROD_PROFILE_VALUE);

        ErrorResponseDto errorResponse = ErrorResponseDto.builder()
                .type(ERROR_CODE_URI + 2)
                .title("Error")
                .detail(isProd ? "An error occurred while processing your request" : exception.getMessage())
                .status(Response.Status.BAD_REQUEST.getReasonPhrase())
                .instance(requestContext.getUriInfo().getPath())
                .build();

        return RestResponse.status(RestResponse.Status.BAD_REQUEST, errorResponse);
    }

}
