package com.kata.temperature.captor.api.mapper;

import com.kata.temperature.captor.api.dto.TemperatureStateDto;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DtoMapper {

    DtoMapper INSTANCE = Mappers.getMapper(DtoMapper.class);

    @Mapping(source = "sensorState", target = "state")
    @Mapping(source = "eventTime", target = "dateTime")
    @Mapping(source = "temperature", target = "value")
    TemperatureStateDto fromTemperatureRequest(TemperatureRequest temperatureRequest);
}
