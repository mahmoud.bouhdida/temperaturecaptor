package com.kata.temperature.captor.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class TemperatureStateDto {

    private String state;
    private int value;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime dateTime;

    @Override
    public String toString() {
        return "HistoryEntryDto{" +
                "state='" + state + '\'' +
                ", value=" + value +
                ", dateTime=" + dateTime +
                '}';
    }
}
