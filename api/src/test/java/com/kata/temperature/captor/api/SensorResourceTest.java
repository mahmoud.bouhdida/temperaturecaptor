package com.kata.temperature.captor.api;

import com.kata.temperature.captor.api.dto.SensorConfigurationDto;
import com.kata.temperature.captor.api.exception.ExceptionMappers;
import com.kata.temperature.captor.api.resource.SensorResource;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(SensorResource.class)
public class SensorResourceTest {

    @DisplayName("When the temperature state is requested, the response format should follow the schema")
    @Test
    public void should_responseValidSchema_when_temperatureStateRequested() {
        given()
                .when().get("/state")
                .then()
                .assertThat()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("temperature-state-schema.json"));
    }

    @DisplayName("When the temperature history is requested, the response format should follow the schema")
    @Test
    public void should_responseValidSchema_when_temperatureHistoryRequested() {
        //Request the temperature state
        given()
                .when().get("/state")
                .then()
                .assertThat()
                .statusCode(200);
        given()
                .when().get("/state")
                .then()
                .assertThat()
                .statusCode(200);

        //Get the history
        given()
                .when().get("/history")
                .then()
                .assertThat()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("temperature-history-schema.json"));
    }

    @DisplayName("When the sensor configuration is set, the response format should follow the schema " +
            "and contain the same data")
    @Test
    public void should_responseValidSchemaSameData_when_sensorConfigurationSet() {
        int min = 20;
        int max = 45;

        SensorConfigurationDto configurationDto = new SensorConfigurationDto();
        configurationDto.setMaximumWarmTemperature(max);
        configurationDto.setMinimumWarmTemperature(min);

        given()
                .contentType("application/json")
                .body(configurationDto)
                .when()
                .post("/configure")
                .then()
                .assertThat()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("config-schema.json"))
                .and()
                .body("maximumWarmTemperature", equalTo(max))
                .body("minimumWarmTemperature", equalTo(min));

    }

    @DisplayName("When the sensor configuration is set with invalid values, " +
            "the error response should follow the error message schema")
    @Test
    public void should_errorResponseValidSchema_when_sensorConfigurationSetInvalidValues() {
        int max = 20;
        int min = 45;

        SensorConfigurationDto configurationDto = new SensorConfigurationDto();
        configurationDto.setMaximumWarmTemperature(max);
        configurationDto.setMinimumWarmTemperature(min);

        given()
                .contentType("application/json")
                .body(configurationDto)
                .when()
                .post("/configure")
                .then()
                .assertThat()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body(matchesJsonSchemaInClasspath("error-message-schema.json"))
                .and()
                .body("title", equalTo(ExceptionMappers.INVALID_CONFIG_ERROR_MESSAGE))
                .body("instance", containsString("/configure"))
                .body("status", equalTo(Response.Status.BAD_REQUEST.getReasonPhrase()));

    }

}