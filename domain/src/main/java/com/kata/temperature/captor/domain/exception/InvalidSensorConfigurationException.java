package com.kata.temperature.captor.domain.exception;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import lombok.Getter;

/**
 * Thrown when there is a validation error with the sensor configuration
 */
public class InvalidSensorConfigurationException extends RuntimeException {

    @Getter
    private final SensorConfiguration sensorConfiguration;


    public InvalidSensorConfigurationException(SensorConfiguration sensorConfiguration, String errorMessage) {
        super(errorMessage);
        this.sensorConfiguration = sensorConfiguration;
    }
}
