package com.kata.temperature.captor.domain.model.enums;

public enum SensorState {
    HOT, COLD, WARM
}
