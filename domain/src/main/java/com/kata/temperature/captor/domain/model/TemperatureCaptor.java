package com.kata.temperature.captor.domain.model;

/**
 * Interface for temperature captors
 */
public interface TemperatureCaptor {

    /**
     * Gets the actual temperature from the captor
     *
     * @return the actuel temperature
     */
    int getCurrentTemperature();
}
