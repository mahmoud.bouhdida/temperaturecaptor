package com.kata.temperature.captor.domain.port.api;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.TemperatureRequest;

import java.util.List;

public interface TemperatureSensorServicePort {

    /**
     * Requests the current temperature status from the sensor
     *
     * @return SensorStatus the sensor status
     */
    TemperatureRequest getCurrentTemperature();

    /**
     * Get the latest sensor status requests
     *
     * @return A list of {@link TemperatureRequest}
     */
    List<TemperatureRequest> getLatestTemperatureRequests();

    /**
     * Set a new limit for the WARM status temperature range.
     * Temperatures below this range will have COLD status.
     * Temperatures above this range will have HOT status.
     *
     * @param warmRangeFrom The minimum temperature for WARM status (inclusive)
     * @param warmRangeTo   The maximum temperature for WARM status (exclusive)
     * @throws InvalidSensorConfigurationException If the sensor configuration is invalid
     */
    void setWarmStatusLimits(int warmRangeFrom, int warmRangeTo) throws InvalidSensorConfigurationException;


    /**
     * Resets the limit for the WARM status temperature range to the default values.
     */
    void resetWarmStatusLimits();

    /**
     * Get the maximum length for the latest temperature requests list
     *
     * @return The maximum history length
     */
    int getMaximumHistoryLength();
}
