package com.kata.temperature.captor.domain.service.implementation;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureCaptor;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.model.TemperatureSensor;
import com.kata.temperature.captor.domain.model.enums.SensorState;
import com.kata.temperature.captor.domain.port.spi.TemperatureSensorPersistencePort;
import com.kata.temperature.captor.domain.service.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class DefaultSensorService implements SensorService {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSensorService.class);
    private static final int DEFAULT_MAX_HISTORY_LENGTH = 15;

    private final TemperatureCaptor temperatureCaptor;

    private final TemperatureSensor temperatureSensor;

    private final TemperatureSensorPersistencePort persistencePort;

    @Inject
    public DefaultSensorService(TemperatureCaptor temperatureCaptor, TemperatureSensor temperatureSensor,
                                TemperatureSensorPersistencePort persistencePort) {
        this.temperatureCaptor = temperatureCaptor;
        this.temperatureSensor = temperatureSensor;
        this.persistencePort = persistencePort;
    }

    @Override
    public TemperatureRequest getSensorStatus() {
        SensorConfiguration sensorConfiguration = getCurrentSensorConfiguration();
        if (!temperatureSensor.getSensorConfiguration().equals(sensorConfiguration)) {
            logger.debug("Sensor configuration needs to be updated");
            temperatureSensor.setSensorConfiguration(sensorConfiguration);
            logger.debug("Sensor configuration was updated successfully");
        }

        logger.debug("Getting sensor status");
        int temperature = temperatureCaptor.getCurrentTemperature();
        logger.debug("The temperature captor returned a value of {}°", temperature);
        SensorState sensorState = temperatureSensor.getStatus(temperature);
        logger.debug("Actual sensor status is {}", sensorState);
        TemperatureRequest temperatureRequest =
                persistencePort.saveTemperatureRequest(TemperatureRequest.forStatus(sensorState, temperature),
                        getCurrentSensorConfiguration());
        logger.debug("The temperature request <{}> was successfully added to the history", temperatureRequest);

        return temperatureRequest;
    }

    @Override
    public List<TemperatureRequest> getTemperatureRequestsHistory() {
        logger.debug("Requesting temperature requests history with a maximum size of {}", DEFAULT_MAX_HISTORY_LENGTH);
        List<TemperatureRequest> requestsHistory = persistencePort
                .loadLatestTemperatureRequests(DEFAULT_MAX_HISTORY_LENGTH);
        logger.debug("A history of size {} was loaded successfully", requestsHistory.size());

        return requestsHistory;
    }

    @Override
    public void updateSensorConfiguration(SensorConfiguration sensorConfiguration)
            throws InvalidSensorConfigurationException {
        logger.debug("Updating the sensor config with: {}", sensorConfiguration);
        temperatureSensor.setSensorConfiguration(sensorConfiguration);
        logger.debug("The sensor configuration was successfully applied");
        persistencePort.saveSensorConfiguration(sensorConfiguration);
        logger.debug("The sensor configuration was successfully persisted");

    }

    @Override
    public SensorConfiguration getCurrentSensorConfiguration() {
        logger.debug("Getting the current sensor config");
        Optional<SensorConfiguration> sensorConfiguration = persistencePort.loadLatestSensorConfiguration();

        if (sensorConfiguration.isPresent()) {
            logger.debug("The sensor configuration <{}> was successfully loaded", sensorConfiguration);
            return sensorConfiguration.get();
        } else {
            logger.debug("There is no persisted sensor configuration. Persisting & applying the default one");
            SensorConfiguration defaultConfiguration = SensorConfiguration.getDefaultConfiguration();
            updateSensorConfiguration(defaultConfiguration);
            return defaultConfiguration;
        }

    }

    @Override
    public int getMaximumHistoryLength() {
        logger.debug("Requesting the maximum history length: {}", DEFAULT_MAX_HISTORY_LENGTH);
        return DEFAULT_MAX_HISTORY_LENGTH;
    }
}
