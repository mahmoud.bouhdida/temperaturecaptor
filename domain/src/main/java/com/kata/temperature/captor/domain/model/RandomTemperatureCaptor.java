package com.kata.temperature.captor.domain.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.util.Random;

/**
 * A simple temperature captor that returns random values
 */
@ApplicationScoped
public class RandomTemperatureCaptor implements TemperatureCaptor {

    private static final Logger logger = LoggerFactory.getLogger(RandomTemperatureCaptor.class);

    private static final int MINIMUM_TEMPERATURE = -10;
    private static final int MAXIMUM_TEMPERATURE = 60;
    private final Random random;

    public RandomTemperatureCaptor() {
        this.random = new Random();
    }

    @Override
    public int getCurrentTemperature() {
        int randomTemperature = random.nextInt(MINIMUM_TEMPERATURE, MAXIMUM_TEMPERATURE);
        logger.debug("Temperature from captor is {}", randomTemperature);
        return randomTemperature;
    }
}
