package com.kata.temperature.captor.domain.port.api.implementation;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.port.api.TemperatureSensorServicePort;
import com.kata.temperature.captor.domain.service.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class DefaultTemperatureSensorServicePort implements TemperatureSensorServicePort {

    private static final Logger logger = LoggerFactory.getLogger(DefaultTemperatureSensorServicePort.class);

    private final SensorService sensorService;

    public DefaultTemperatureSensorServicePort(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @Override
    public TemperatureRequest getCurrentTemperature() {
        logger.debug("Requesting current sensor status");
        TemperatureRequest temperatureRequest = sensorService.getSensorStatus();
        logger.info("Current temperature request is {}", temperatureRequest);
        return temperatureRequest;
    }

    @Override
    public List<TemperatureRequest> getLatestTemperatureRequests() {
        logger.info("Requesting the temperature request history");
        List<TemperatureRequest> history = sensorService.getTemperatureRequestsHistory();
        logger.info("History of length {} retrieved successfully", history.size());
        return history;
    }

    @Override
    public void setWarmStatusLimits(int warmRangeFrom, int warmRangeTo) throws InvalidSensorConfigurationException {
        logger.info("Overriding WARM status limits to [{},{}[", warmRangeFrom, warmRangeTo);
        sensorService.updateSensorConfiguration(SensorConfiguration.forWarmRange(warmRangeFrom, warmRangeTo));
        logger.info("WARM status limits were successfully overridden");
    }

    @Override
    public void resetWarmStatusLimits() {
        logger.info("Resetting WARM status limits default value");
        SensorConfiguration defaultConfig = SensorConfiguration.getDefaultConfiguration();
        sensorService.updateSensorConfiguration(defaultConfig);
        logger.info("WARM status limits were successfully reset to [{},{}[",
                defaultConfig.getMinimumTemperatureWarmStatus(), defaultConfig.getMaximumTemperatureWarmStatus());
    }

    @Override
    public int getMaximumHistoryLength() {
        logger.info("Getting maximum history length");
        int maximumLength = sensorService.getMaximumHistoryLength();
        logger.info("Maximum length is {}", maximumLength);
        return maximumLength;
    }
}
