package com.kata.temperature.captor.domain.port.spi;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;

import java.util.List;
import java.util.Optional;

public interface TemperatureSensorPersistencePort {

    /**
     * Loads the temperature request history
     *
     * @param size The maximum entries to load if available
     * @return List of temperature requests
     */
    List<TemperatureRequest> loadLatestTemperatureRequests(int size);

    /**
     * Saves the temperature request to the history
     *
     * @param temperatureRequest  The {@link TemperatureRequest} to save
     * @param sensorConfiguration The {@link SensorConfiguration} used to get the status
     * @return The saved {@link TemperatureRequest}
     */
    TemperatureRequest saveTemperatureRequest(TemperatureRequest temperatureRequest,
                                              SensorConfiguration sensorConfiguration);

    /**
     * Loads the latest sensor configuration
     *
     * @return An {@link Optional} that may contain the latest {@link SensorConfiguration}
     */
    Optional<SensorConfiguration> loadLatestSensorConfiguration();

    /**
     * Saves a new sensor configuration
     *
     * @param sensorConfiguration The {@link SensorConfiguration} to save
     * @return The saved {@link SensorConfiguration}
     */
    SensorConfiguration saveSensorConfiguration(SensorConfiguration sensorConfiguration);
}
