package com.kata.temperature.captor.domain.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * Model class for temperature sensor configuration
 */
@Getter
@Setter
public class SensorConfiguration {
    private static final int DEFAULT_MINIMUM_TEMPERATURE_WARM_STATUS = 22;
    private static final int DEFAULT_MAXIMUM_TEMPERATURE_WARM_STATUS = 40;

    /**
     * The minimum temperature for WARM status (inclusive)
     */
    private int minimumTemperatureWarmStatus;

    /**
     * The maximum temperature for WARM status (exclusive)
     */
    private int maximumTemperatureWarmStatus;

    /**
     * Get a new instance of {@link SensorConfiguration} with new limit for the WARM status temperature range.
     * Temperatures below this range will have COLD status.
     * Temperatures above this range will have HOT status.
     *
     * @param warmRangeFrom The minimum temperature for WARM status (inclusive)
     * @param warmRangeTo   The maximum temperature for WARM status (exclusive)
     */
    public static SensorConfiguration forWarmRange(int warmRangeFrom, int warmRangeTo) {
        SensorConfiguration sensorConfiguration = new SensorConfiguration();
        sensorConfiguration.maximumTemperatureWarmStatus = warmRangeTo;
        sensorConfiguration.minimumTemperatureWarmStatus = warmRangeFrom;

        return sensorConfiguration;
    }

    /**
     * Get a new instance of {@link SensorConfiguration} with the default limits for the WARM status temperature range.
     */
    public static SensorConfiguration getDefaultConfiguration() {
        SensorConfiguration sensorConfiguration = new SensorConfiguration();
        sensorConfiguration.maximumTemperatureWarmStatus = DEFAULT_MAXIMUM_TEMPERATURE_WARM_STATUS;
        sensorConfiguration.minimumTemperatureWarmStatus = DEFAULT_MINIMUM_TEMPERATURE_WARM_STATUS;

        return sensorConfiguration;
    }

    @Override
    public String toString() {
        return "SensorConfiguration{" +
                "minimumTemperatureWarmStatus=" + minimumTemperatureWarmStatus +
                ", maximumTemperatureWarmStatus=" + maximumTemperatureWarmStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorConfiguration that = (SensorConfiguration) o;
        return minimumTemperatureWarmStatus == that.minimumTemperatureWarmStatus
                && maximumTemperatureWarmStatus == that.maximumTemperatureWarmStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(minimumTemperatureWarmStatus, maximumTemperatureWarmStatus);
    }
}
