package com.kata.temperature.captor.domain.model;

import com.kata.temperature.captor.domain.model.enums.SensorState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Model class for a sensor status request
 */
@Getter
@NoArgsConstructor
@Setter
public class TemperatureRequest {

    private SensorState sensorState;

    private int temperature;

    private LocalDateTime eventTime;

    /**
     * @param status      The sensor status to include in the request
     * @param temperature The temperature value
     * @return a new instance of a TemperatureRequest
     */
    public static TemperatureRequest forStatus(SensorState status, int temperature) {
        TemperatureRequest temperatureRequest = new TemperatureRequest();
        temperatureRequest.sensorState = status;
        temperatureRequest.eventTime = LocalDateTime.now();
        temperatureRequest.temperature = temperature;

        return temperatureRequest;
    }

    @Override
    public String toString() {
        return "TemperatureRequest{" +
                "sensorStatus=" + sensorState +
                ", temperature=" + temperature +
                ", localDateTime=" + eventTime +
                '}';
    }
}
