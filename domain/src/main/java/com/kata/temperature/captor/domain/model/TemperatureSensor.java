package com.kata.temperature.captor.domain.model;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.enums.SensorState;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;

/**
 * The sensor that will get the temperature from the captor
 */
@ApplicationScoped
public class TemperatureSensor {

    private static final Logger logger = LoggerFactory.getLogger(TemperatureSensor.class);

    private static final int MAXIMUM_PERMITTED_WARM_TEMPERATURE = 50;
    private static final int MINIMUM_PERMITTED_WARM_TEMPERATURE = 10;

    @Getter
    private SensorConfiguration sensorConfiguration;

    public TemperatureSensor() {
        sensorConfiguration = SensorConfiguration.getDefaultConfiguration();
    }

    /**
     * Get the actual status of the temperature sensor
     *
     * @return The status
     */
    public SensorState getStatus(int temperature) {
        logger.debug("Getting sensor status for temperature {}°", temperature);
        if (temperature >= sensorConfiguration.getMaximumTemperatureWarmStatus()) {
            return SensorState.HOT;
        } else if (temperature < sensorConfiguration.getMinimumTemperatureWarmStatus()) {
            return SensorState.COLD;
        } else {
            return SensorState.WARM;
        }
    }

    public void setSensorConfiguration(SensorConfiguration sensorConfiguration) throws InvalidSensorConfigurationException {
        validateConfiguration(sensorConfiguration);
        this.sensorConfiguration = sensorConfiguration;
    }

    /**
     * Validate the configuration details
     *
     * @param sensorConfiguration The {@link SensorConfiguration} to validate
     * @throws InvalidSensorConfigurationException if the configuration details are invalid
     */
    private void validateConfiguration(SensorConfiguration sensorConfiguration) throws InvalidSensorConfigurationException {
        String errorMessage = "";

        if (sensorConfiguration.getMinimumTemperatureWarmStatus()
                > sensorConfiguration.getMaximumTemperatureWarmStatus()) {
            errorMessage = "The WARM range minimum value cannot be greater than the maximum one";
        }

        if (sensorConfiguration.getMinimumTemperatureWarmStatus()
                == sensorConfiguration.getMaximumTemperatureWarmStatus()) {
            errorMessage = "The WARM range values cannot be identical";
        }

        if (sensorConfiguration.getMinimumTemperatureWarmStatus() < MINIMUM_PERMITTED_WARM_TEMPERATURE) {
            errorMessage = String.format("The minimum temperature for a warm status cannot be less than %s",
                    MINIMUM_PERMITTED_WARM_TEMPERATURE);
        }

        if (sensorConfiguration.getMaximumTemperatureWarmStatus() > MAXIMUM_PERMITTED_WARM_TEMPERATURE) {
            errorMessage = String.format("The maximum temperature for a warm status cannot be more than %s",
                    MAXIMUM_PERMITTED_WARM_TEMPERATURE);
        }

        if (!errorMessage.isEmpty()) {
            logger.error(errorMessage);
            throw new InvalidSensorConfigurationException
                    (sensorConfiguration, errorMessage);
        }
    }


}
