package com.kata.temperature.captor.domain.service;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;

import java.util.List;

public interface SensorService {

    /**
     * @return the current temperature sensor status
     */
    TemperatureRequest getSensorStatus();

    /**
     * @return the latest temperature requests
     */
    List<TemperatureRequest> getTemperatureRequestsHistory();

    /**
     * Updates the sensor configuration
     *
     * @param sensorConfiguration The new service configuration
     * @throws InvalidSensorConfigurationException If the sensor configuration is invalid
     */
    void updateSensorConfiguration(SensorConfiguration sensorConfiguration) throws InvalidSensorConfigurationException;

    /**
     * Get the current configuration for the temperature sensor
     *
     * @return The current {@link SensorConfiguration}
     */
    SensorConfiguration getCurrentSensorConfiguration();

    /**
     * @return The maximum length of the sensor status requests list
     */
    int getMaximumHistoryLength();
}
