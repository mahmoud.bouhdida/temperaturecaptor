package com.kata.temperature.captor.domain.stub;

import com.kata.temperature.captor.domain.model.SensorConfiguration;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.port.spi.TemperatureSensorPersistencePort;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class TemperatureSensorPersistenceStub implements TemperatureSensorPersistencePort {

    private final List<TemperatureRequest> requestsHistory;
    private final List<SensorConfiguration> sensorConfigurations;

    public TemperatureSensorPersistenceStub() {
        requestsHistory = new ArrayList<>();
        sensorConfigurations = new ArrayList<>();
    }

    @Override
    public List<TemperatureRequest> loadLatestTemperatureRequests(int size) {
        if (requestsHistory.size() <= size) {
            return requestsHistory;
        }

        return requestsHistory
                .stream()
                .skip(requestsHistory.size() - size)
                .collect(Collectors.toList());
    }

    @Override
    public TemperatureRequest saveTemperatureRequest(TemperatureRequest temperatureRequest,
                                                     SensorConfiguration sensorConfiguration) {
        requestsHistory.add(temperatureRequest);
        return temperatureRequest;
    }

    @Override
    public Optional<SensorConfiguration> loadLatestSensorConfiguration() {
        if (sensorConfigurations.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(sensorConfigurations.get(sensorConfigurations.size() - 1));
        }
    }

    @Override
    public SensorConfiguration saveSensorConfiguration(SensorConfiguration sensorConfiguration) {
        sensorConfigurations.add(sensorConfiguration);
        return sensorConfiguration;
    }
}
