package com.kata.temperature.captor.domain;

import com.kata.temperature.captor.domain.exception.InvalidSensorConfigurationException;
import com.kata.temperature.captor.domain.model.TemperatureCaptor;
import com.kata.temperature.captor.domain.model.TemperatureRequest;
import com.kata.temperature.captor.domain.model.TemperatureSensor;
import com.kata.temperature.captor.domain.model.enums.SensorState;
import com.kata.temperature.captor.domain.port.api.TemperatureSensorServicePort;
import com.kata.temperature.captor.domain.port.spi.TemperatureSensorPersistencePort;
import com.kata.temperature.captor.domain.service.SensorService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@QuarkusTest
@ExtendWith(MockitoExtension.class)
public class TemperatureSensorTests {

    @InjectMock
    private TemperatureCaptor temperatureCaptor;

    @Inject
    TemperatureSensorServicePort temperatureSensorServicePort;

    @Inject
    TemperatureSensor temperatureSensor;

    @Inject
    TemperatureSensorPersistencePort persistencePort;

    @Mock
    SensorService sensorService;

    AutoCloseable openMocks;

    @BeforeEach
    void setUp() {
        openMocks = MockitoAnnotations.openMocks(this);
    }

    @ParameterizedTest(name = "When the temperature is {0}°, the sensor status should be {1}")
    @MethodSource("provideDefaultSensorStatusTestData")
    public void should_sensorStatusCorrect_when_temperatureReturnedFromCaptor(int temperature, SensorState expectedStatus) {
        given(temperatureCaptor.getCurrentTemperature())
                .willReturn(temperature);

        TemperatureRequest temperatureRequest = temperatureSensorServicePort.getCurrentTemperature();
        assertEquals(expectedStatus, temperatureRequest.getSensorState(),
                "The actual sensor status is not " + expectedStatus.name() + " for " + temperature + "°");
    }

    @DisplayName("When the temperature history is requested, only the 15 latest requests should be returned")
    @Test
    public void should_returnLatest15TemperatureRequests_when_historyRequested() {
        //Simulate N (>15) requests
        int numberOfRequests = 20;
        int baseTemperature = 35; //First 5 WARM, last 15 HOT
        for (int i = 0; i < numberOfRequests; i++) {
            given(temperatureCaptor.getCurrentTemperature())
                    .willReturn(baseTemperature + i);
            temperatureSensorServicePort.getCurrentTemperature();
        }

        //Get the history
        List<TemperatureRequest> temperatureRequestList = temperatureSensorServicePort.getLatestTemperatureRequests();

        //Assert history length
        assertEquals(temperatureSensorServicePort.getMaximumHistoryLength(), temperatureRequestList.size(),
                "History length is invalid");

        //Assert first N-15 requests not available
        boolean warmExists = temperatureRequestList
                .stream()
                .anyMatch(req -> SensorState.WARM.equals(req.getSensorState()));

        assertFalse(warmExists, "The history is not returning the latest 15 requests");
    }

    @ParameterizedTest(name = "When the temperature is {0}° and the WARM range is redefined to [{2}, {3}[, " +
            "the sensor status should be {1}")
    @MethodSource("provideCustomSensorStatusTestData")
    public void should_sensorStatusCorrect_when_sensorStatusLimitsRedefined(int temperature, SensorState expectedStatus,
                                                                            int warmRangeFrom, int warmRangeTo) {
        given(temperatureCaptor.getCurrentTemperature())
                .willReturn(temperature);

        temperatureSensorServicePort.setWarmStatusLimits(warmRangeFrom, warmRangeTo);

        TemperatureRequest temperatureRequest = temperatureSensorServicePort.getCurrentTemperature();
        assertEquals(expectedStatus, temperatureRequest.getSensorState(),
                String.format("The actual sensor status is not %s for %s°. WARM range is [%s, %s[",
                        expectedStatus.name(), temperature, warmRangeFrom, warmRangeTo));
    }

    @ParameterizedTest(name = "When the temperature is {0}° and the WARM range is redefined to [{2}, {3}[ then reset, " +
            "the sensor status should be {1}")
    @MethodSource("provideCustomThenDefaultSensorStatusTestData")
    public void should_sensorStatusCorrect_when_sensorStatusLimitsRedefinedThenReset(int temperature, SensorState expectedStatus,
                                                                                     int warmRangeFrom, int warmRangeTo) {
        given(temperatureCaptor.getCurrentTemperature())
                .willReturn(temperature);

        temperatureSensorServicePort.setWarmStatusLimits(warmRangeFrom, warmRangeTo);
        temperatureSensorServicePort.resetWarmStatusLimits();

        TemperatureRequest temperatureRequest = temperatureSensorServicePort.getCurrentTemperature();
        assertEquals(expectedStatus, temperatureRequest.getSensorState(),
                String.format("The actual sensor status is not %s for %s°. WARM range is [%s, %s[",
                        expectedStatus.name(), temperature, warmRangeFrom, warmRangeTo));
    }

    @ParameterizedTest(name = "When the sensor WARM range is redefined to [{0}, {1}[, " +
            "the change shouldn't be accepted and an exception should be thrown")
    @MethodSource("provideInvalidWarmRangeTestData")
    public void should_throwException_when_warmRangeInvalid(int warmRangeFrom, int warmRangeTo) {
        assertThrows(InvalidSensorConfigurationException.class,
                () -> temperatureSensorServicePort.setWarmStatusLimits(warmRangeFrom, warmRangeTo),
                String.format("The WARM status range [%s, %s[ is invalid. An exception should be thrown",
                        warmRangeFrom, warmRangeTo));
    }

    @AfterEach
    void tearDown() throws Exception {
        openMocks.close();
    }

    /**
     * Provide test data for invalid WARM status range
     *
     * @return Steam of test arguments
     */
    private static Stream<Arguments> provideInvalidWarmRangeTestData() {
        return Stream.of(
                Arguments.of(0, 30),
                Arguments.of(25, 100),
                Arguments.of(30, 25),
                Arguments.of(24, 24)
        );
    }

    /**
     * Provide test data for default sensor status behavior
     *
     * @return Steam of test arguments
     */
    private static Stream<Arguments> provideDefaultSensorStatusTestData() {
        return Stream.of(
                Arguments.of(45, SensorState.HOT),
                Arguments.of(20, SensorState.COLD),
                Arguments.of(25, SensorState.WARM)
        );
    }

    /**
     * Provide test data for customized sensor status behavior
     *
     * @return Steam of test arguments
     */
    private static Stream<Arguments> provideCustomSensorStatusTestData() {
        int warmRangeFrom = 30;
        int warmRangeTo = 50;
        return Stream.of(
                Arguments.of(50, SensorState.HOT, warmRangeFrom, warmRangeTo),
                Arguments.of(25, SensorState.COLD, warmRangeFrom, warmRangeTo),
                Arguments.of(30, SensorState.WARM, warmRangeFrom, warmRangeTo)
        );
    }

    /**
     * Provide test data for sensor status behavior that was customized and then reset
     *
     * @return Steam of test arguments
     */
    private static Stream<Arguments> provideCustomThenDefaultSensorStatusTestData() {
        int warmRangeFrom = 30;
        int warmRangeTo = 50;
        return Stream.of(
                Arguments.of(45, SensorState.HOT, warmRangeFrom, warmRangeTo),
                Arguments.of(20, SensorState.COLD, warmRangeFrom, warmRangeTo),
                Arguments.of(25, SensorState.WARM, warmRangeFrom, warmRangeTo)
        );
    }
}
