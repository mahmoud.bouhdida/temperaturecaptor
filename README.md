# Temperature Captor

## Objectif

Le but est d'écrire un API REST simple pour un capteur de température.
Grâce à ce service Web, on peut connaitre l'état actuel de la température, l'historique et redéfinir les limits de ces
états.

Les détails sont décrits ci-dessous :

- Je veux que mon sensor récupère la température provenant du composant TemperatureCaptor (renvoi la température en
  °C)<br/>
- Je veux que l'état de mon Sensor soit à "HOT" lorsque la température captée est suppérieure ou égale a 40 °C.<br/>
- Je veux l'état de mon Sensor soit à "COLD" lorsque la température captée est inferieur a 22 °C.<br/>
- Je veux l'état de mon Sensor soit à "WARM" lorsque la température captée est entre 22 et inferieur à 40 °C.<br/>
- Je veux récuperer l'historique des 15 dernieres demandes des températures.<br/>
- Je veux pouvoir redefinir les limites pour "HOT", "COLD", "WARM"<br/>

Les approches qui seront utilisées dans ce projects sont:

- Le DDD (Domain-Driven Design) à travers l'architecture hexagonale.

## Organisation du projet

Le projet est organisé en 3 modules:

- ``` domain``` : Contient le modèle metier isolé derrière les ports (architecture hexagonale).
- ``` api``` : Contient l'adaptateur API qui expose l'application à travers un API REST.
- ``` persistence``` : Contient l'adaptateur de persistence de l'application.

## Stack technique

- **Language** : Java 17
- **Gestion de dépendances** : Maven
- **Base de données** : PostgreSQL
- **Container**: Docker
- **Framework API REST** : Quarkus
- **Documentation API** : Swagger/OpenAPI
- **Framework de test unitaire** : JUnit

## Comment exécuter l'application

```bash
mvn clean package -Dquarkus.profile=dev

docker-compose build

docker-compose up
```

L'application sera prête sous ``` http://localhost:8080/```

##### Comment exécuter les tests ?

```bash
mvn clean test
```

## Endpoints

- ``` GET {APP_HOST}/sensor/status``` : Etat actuel de la température
- ``` GET {APP_HOST}/sensor/history``` : Historique des demandes des états de la température
- ``` POST {APP_HOST}/sensor/configure``` : Redéfinition des limites pour "WARM"
- ``` GET {APP_HOST}/q/docs/``` : Documentation avec Swagger
